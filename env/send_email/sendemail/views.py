from django.shortcuts import render
from .models import Contact
from .forms import ContactForm
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponse
from django.http import HttpResponseRedirect
from django.conf import settings


# Create your views here.

from django.core.mail import send_mail



def home(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
 
            contact = form.save(commit=False)
            contact.author = request.user
            contact.save()
            data = request.POST.copy()
            name = data['name']
            email_address = data['email_address']

            subject = request.POST.get('subject')
            message = request.POST.get('message')
            sender = '%s <%s>' % (name, email_address)
            send_mail(subject, message, sender, [settings.DEFAULT_CONTACT_EMAIL])
            return redirect('/', pk=contact.pk)
    else:
        form = ContactForm()
    return render(request, 'sendemail/home.html', {'form': form})

