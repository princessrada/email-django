# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('sendemail', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='subject',
            field=models.CharField(default=datetime.datetime(2015, 2, 27, 6, 40, 6, 117112, tzinfo=utc), max_length=200),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contact',
            name='email_address',
            field=models.EmailField(max_length=200),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='contact',
            name='message',
            field=models.TextField(max_length=200),
            preserve_default=True,
        ),
    ]
