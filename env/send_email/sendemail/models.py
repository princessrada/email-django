from django.db import models
from django.utils import timezone

class Contact(models.Model):
    author = models.ForeignKey('auth.User')
    name = models.CharField(max_length=200)
    subject = models.CharField(max_length=200)
    email_address = models.EmailField(max_length=200)
    message = models.TextField(max_length=200)
    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.name